import { PracticeTestTests } from "../../tests/practice/PracticeTestTests";

const practiceTestTests = new PracticeTestTests()

Given("Opened practice section", () => {
    practiceTestTests.openPracticeSection()
})

Then("Selected one subject from available subjects", () => {
    practiceTestTests.selectRandomSubject()
})

And("Selected one chapter and clicked on start test button", () => {
    practiceTestTests.selectRandomChapter()
})

Then("Selected number of questions and started the test", () => {
    practiceTestTests.selectNumberOfQuestionsAndStartTest()
})

And("Attempted the test", () => {
    practiceTestTests.attemptTest()
})

Then("Validated the result page", () => {
    practiceTestTests.validateResultPage()
})