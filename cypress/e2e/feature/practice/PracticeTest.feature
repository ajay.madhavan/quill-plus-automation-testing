Feature: Attempt practice test
Background: User is on home page
    Given I visited login page
    Then I logged in with "9930995548" and default OTP

    Scenario: User attempting practice test
    Given Opened practice section
    Then Selected one subject from available subjects
    And Selected one chapter and clicked on start test button
    Then Selected number of questions and started the test
    And Attempted the test
    Then Validated the result page