/// <reference types="Cypress" />

import { RegisterConstants } from "./constants/RegisterConstants";
import { faker } from "@faker-js/faker";

export class RegisterValidation {
    enterName(name) {
        cy.get(RegisterConstants.NAME).type(name)
    }

    enterEmail(email) {
        if(email === "random") {
            cy.get(RegisterConstants.EMAIL).type(faker.internet.email())
        } else {
            cy.get(RegisterConstants.EMAIL).type(email)
        }
    }

    selectDOB(dob) {
        const [year, month, date] = dob.split('-')
        cy.get(RegisterConstants.DOB).click()
        cy.get(RegisterConstants.CALENDAR_YEAR).type(year)
        cy.get(RegisterConstants.CALENDAR_MONTH).select(parseInt(month)-1)
        cy.xpath(`//div[@class='dayContainer']//span[normalize-space()='${date}'][@class='flatpickr-day']`).click()
    }

    selectGender(gender) {
        cy.xpath(RegisterConstants.GENDER_PARENT_DIV).trigger('mousedown').type(gender+"{enter}")
    }

    selectState(state) {
        cy.intercept("GET", RegisterConstants.FETCH_CITIES_ROUTE).as("stateRoute")
        cy.xpath(RegisterConstants.STATE_PARENT_DIV).trigger('mousedown').type(state+"{enter}")
    }

    selectCity(city) {
        cy.wait("@stateRoute").then(() => {
            cy.xpath(RegisterConstants.STATE_PARENT_DIV).trigger('mousedown').type(city+"{enter}")
        })
    }

    clickOnNextButton() {
        cy.contains(RegisterConstants.NEXT_BUTTON_TEXT).click()
    }
}