/// <reference types="Cypress" />

import { SidebarConstants } from "./constants/SidebarConstants";

export class SidebarValidation {
    goToPracticeSection() {
        cy.get(SidebarConstants.PRACTICE_LINK).contains(SidebarConstants.PRACTICE_TEXT).click()
    }
}