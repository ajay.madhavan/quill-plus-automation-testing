export class LoginConstants {
    static LOGIN_URL = "/login"
    static PHONE_INPUT = "#phone"
    static PROCEED_TEXT = "Proceed"
    static OTP_INPUT = ".auth-login-form.mt-2>div:nth-child(2)>div>input:nth-child(1)"
    static DEFAULT_OTP = "998877"
    static STATUS_CODE_404 = 404
    static USER_DOES_NOT_EXIST_MESSAGE = "User with this phone number does not exist"
    static IS_USER_REGISTERED_URL = "**/users/is-user-registered"
    static VERIFY_OTP_URL = "**/users/verify-otp"
}