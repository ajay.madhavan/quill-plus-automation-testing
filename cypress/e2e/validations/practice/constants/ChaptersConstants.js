export class ChaptersConstants {
    static SUBJECT_HEADING = "h1"
    static CHAPTERS = "[data-test-id^='practise-test-chapter-']"
    static VOLUME_ACCORDIAN_TRIGGER = "[data-test-id^='practise-test-volume-'] span.toggle-collapse"
    static START_TEST_BUTTON = "[data-test-id='practise-test-start-button']"
    static NUMBER_OF_QUESTIONS = "[data-test-id^='practise-test-total-questions-']"
    static CONFIRM_SUBMIT_BUTTON = "[data-test-id='practise-test-no-questions-submit']"
}