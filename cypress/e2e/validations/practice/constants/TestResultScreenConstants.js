export class TestResultScreenConstants {
    static SCORE = "[data-test-id='customized-test-analytics-marks']"
    static TIME_CHART_VALUE = "//div[@class='card']/div/div[normalize-space()='Time']/parent::div/parent::div/div[@class='card-body']//*[@class='apexcharts-pie']/*[@class='apexcharts-datalabels-group']/*[2]"
}