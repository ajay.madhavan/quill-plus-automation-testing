/// <reference types="Cypress" />

import { Utils } from "../../../../utils/Utils"
import { ChaptersConstants } from "../constants/ChaptersConstants"

export class ChaptersValidation {
    selectRandomChapter() {
        cy.get(ChaptersConstants.VOLUME_ACCORDIAN_TRIGGER).then(($volumes) => {
            if($volumes.length > 1) {
                for(let i=1; i<$volumes.length; i++) {
                    cy.wrap($volumes).eq(i).click()
                }
            }
        })

        cy.get(ChaptersConstants.CHAPTERS).then(($chapters) => {
            let randomNumber = Utils.getRandomInt(0, $chapters.length-1)
            cy.wrap($chapters).eq(randomNumber).click()
        })
    }

    clickOnStartTestButton() {
        cy.get(ChaptersConstants.START_TEST_BUTTON).should("not.be.disabled").click()
    }

    selectNumberOfQuestions() {
        let randomNumber = Utils.getRandomInt(0, 2)
        cy.get(ChaptersConstants.NUMBER_OF_QUESTIONS).eq(randomNumber).click()
    }

    clickOnConfirmSubmit() {
        cy.get(ChaptersConstants.CONFIRM_SUBMIT_BUTTON).click()
    }

    validateSelectedSubject(subject) {
        cy.get(ChaptersConstants.SUBJECT_HEADING).invoke("text").should("equal", subject)
    }
}