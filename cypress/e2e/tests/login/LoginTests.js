import {LoginValidation} from "../../validations/login/LoginValidation";

const login = new LoginValidation()
export class LoginTests {
    visitLoginPage() {
        login.visitLoginPage()
    }

    loginWithValidCredentials(phoneNumber) {
        login.enterPhone(phoneNumber)
        login.clickOnProceedButton()
        login.enterOTP()
        login.clickOnProceedButton()
    }

    loginWithWrongOTPAndValidateErrorMessage() {
        login.enterPhone("9930995548")
        login.clickOnProceedButton()
        login.enterOTP()
        login.interceptVerifyOTPRequest()
        login.clickOnProceedButton()
        login.validateWrongOTPMessage()
    }
}