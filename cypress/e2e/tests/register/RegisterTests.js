import { LoginValidation } from "../../validations/login/LoginValidation"
import { RegisterValidation } from "../../validations/register/RegisterValidation"
import { faker } from "@faker-js/faker";

const login = new LoginValidation()
const register = new RegisterValidation()

export class RegisterTests {
    loginWithRandomPhoneNumber() {
        login.enterPhone(faker.string.numeric(10))
        login.interceptIsUserRegisteredRequest()
        login.clickOnProceedButton()
        login.validateIsUserRegisteredResponse()
        login.enterOTP()
        login.clickOnProceedButton()        
    }

    fillRegistrationDetails(datatable) {
        const rows = datatable.raw()
        const tableHeadings = rows[0]

        for(let i=1; i<rows.length; i++) {
            register.enterName(rows[i][tableHeadings.indexOf("name")])
            register.enterEmail(rows[i][tableHeadings.indexOf("email")])
            register.selectDOB(rows[i][tableHeadings.indexOf("dob")])
            register.selectGender(rows[i][tableHeadings.indexOf("gender")])
            register.selectState(rows[i][tableHeadings.indexOf("state")])
            register.selectCity(rows[i][tableHeadings.indexOf("city")])
        }
        register.clickOnNextButton()
    }
}