import { ChaptersValidation } from "../../validations/practice/chapters/ChaptersValidation";
import { TestResultScreenValidation } from "../../validations/practice/resultScreen/TestResultScreenValidation";
import { SubjectValidation } from "../../validations/practice/subjects/SubjectsValidation";
import { TestScreenValidation } from "../../validations/practice/testScreen/TestScreenValidation";
import { SidebarValidation } from "../../validations/sidebar/SidebarValidation";

const sidebar = new SidebarValidation()
const subject = new SubjectValidation()
const chapters = new ChaptersValidation()
const testScreen = new TestScreenValidation()
const testResultScreen = new TestResultScreenValidation()

export class PracticeTestTests {
    openPracticeSection() {
        sidebar.goToPracticeSection()
    }

    selectRandomSubject() {
        subject.selectOneSubject().then((subjectName) => {
            chapters.validateSelectedSubject(subjectName)
        })
    }

    selectRandomChapter() {
        chapters.selectRandomChapter()
        chapters.clickOnStartTestButton()
    }

    selectNumberOfQuestionsAndStartTest() {
        chapters.selectNumberOfQuestions()
        chapters.clickOnConfirmSubmit()
    }

    attemptTest() {
        testScreen.attemptTest()
    }
    
    validateResultPage() {
        testScreen.waitForTestToSubmit().then(() => {
            testResultScreen.validateScore(testScreen.getCorrectQuestionCount(), testScreen.getTotalQuestionCount())
            testResultScreen.validateTotalTimeTakenChart(testScreen.getTotalTimeTaken())
        })
    }
}